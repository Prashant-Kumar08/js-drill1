function problem2(inventory){
	let emptyArr = [];

	if(!inventory || typeof inventory !=='object'){
		return emptyArr;
	}

	
	if(Object.keys(inventory).length === 0){
		return emptyArr;
	}

	if(inventory.length === 0){
		return emptyArr;
	}

    return inventory[inventory.length-1];
};


module.exports = problem2; 
